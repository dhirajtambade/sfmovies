
AUTHOR - Dhiraj Tambade
DATE - 07/11/2015
LINKEDIN PROFILE - https://in.linkedin.com/in/dhirajtambade
-----------------------------------------------------------


PROBLEM STATEMENT
-----------------
Create a service that shows on a map where movies have been filmed in San Francisco.
The user should be able to filter the view using autocompletion search.


SOLUTION
--------

Visit - http://stark-spire-4846.herokuapp.com/film/

Django Application is developed where user will select movie name from autocomplete.
On selection, locations where that movie is filmed are marked on Map.


LINK TO REPOSITORY
------------------
https://bitbucket.org/dhirajtambade/sfmovies


TECHNOLOGIES USED
-----------------
Web Framework : Python-Django
CSS Framework : Materialize (https://github.com/Dogfalo/materialize)
Front-End     : Javascript and JQuery
REST APIs     : Django REST Framework (https://github.com/tomchristie/django-rest-framework)
MAP API       : Google Map Chart (https://developers.google.com/chart/interactive/docs/gallery/map)
                Google Geo Coding API for langitude and latidude informtion for given address
Database      : PostgreSQL
Deployment    : Heroku


HOW IT WORKS
------------
For all filming locations, corresponding latitude and longitude information is collected using Google GeoCoding API and stored into database.
When user selects a movie from autocomplete, information like year of release, directors etc is updated on UI and filming locations are marked on Map using their latitude and longitude information.


TRADE-OFFS
----------

* Why Lat-Long pairs when Address string can be used for marking locations ?

    There are 2 reasons for this.

    1.  Address strings we are using are not properly formatted so we get some wrong markings on Map and we cannot filter out
        such results.
        So We are marking locations only when their longitude is less than -121 (San Francisco Area) to prevent wrong results.

    2.  The Lat-Long pairs option loads maps much faster than string address.
        (Ref: https://developers.google.com/chart/interactive/docs/gallery/map)


THINGS THAT COULD HAVE BEEN DONE DIFFERENTLY
--------------------------------------------
1. Data Curation - Currently We have correct lat-long information for 85% locations. Using different APIs, this can be improved.
2. MVC framework for Front-End. I have never worked on Front-End MVC thats why I used Javascript and JQuery.


EXPERIENCE IN TECHNOLOGIES USED
-----------------------------
    Python - 2.5 Years
    Django - 1.5 Years
    Javascript and JQuery - 6 Months
    Django REST Framework - Using for the first time
    Materialize CSS Framework - Using for the first time
    MAP APIs - Using for the first time
    Heroku - Using for the first time


CODE WRITTEN
------------
    filmlocations/models.py - Dababase Models
    filmlocations/serializers.py - Serialization of objects
    filmlocations/views.py - Rest API
    filmlocations/data/lat_long.py - Collection of Lat-Long Information
    filmlocations/data/load_data.py - Load data to database
    filmlocations/tests.py - Test for API
    static/js/sfmovies.js - Javascript/JQuery code required for Front-End
    templates/index.html - Home Page Template of Application
