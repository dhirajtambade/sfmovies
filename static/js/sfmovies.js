function drawMap(locations){
    options.zoomLevel = 'automatic';
    options.showTip = true;

    locArray = [['Lat', 'Long', 'Name']],
    $.each(locations, function(index, loc){
        lat = parseFloat(loc.latitude);
        lng = parseFloat(loc.longitude);
        // exclude address with wrong lat-long information or no information
        if (lng < -121){
            locArray.push([lat, lng, loc.location]);
        };
    })

    // if no location is correctly geocoded, load initial map
    if (locArray.length == 1){
        Materialize.toast("Sorry, We couldn't find any location on map", 4000);
        locArray = [['Address'],['San Francisco, CA, US']]
        options.zoomLevel = 12;
        options.showTip = false;
    }

    filmLocations = new google.visualization.arrayToDataTable(locArray);
    map = new google.visualization.Map(document.getElementById('map'));
    map.draw(filmLocations, options);
}

function populateInfo(info){

    var locHtml = "<ul>";
    $.each(info.locations, function(index, loc){
        locHtml += "<li>" + (index + 1) + ' - ' + loc.location + "</li>";
    });
    locHtml += "</ul>";

    $("#location").html(locHtml);
    $("#year").text(info.year);
    $("#director").text(info.director);
    $("#company").text(info.company);
    $("#writers").text(info.writers);
    $("#actors").text(info.actors);

    var locCard = $("#location").closest('li');
    if ($(locCard).hasClass("active") == false){
        $(locCard).find(".collapsible-header").click();
    }
}

$("#movie_name").autocomplete({
    source: function(request, response) {
        $.ajax({
            url: '/film/autocomplete/',
            data :{
                "term": request.term,
            },
            success:function(data){
                response(data);
            },
            error:function(data){
                Materialize.toast('Something went wrong !', 4000);
            }
        });
    },
    change: function(event, ui){
        if (ui.item==null){
            $("#movie_name").val('');
        }
    },
    select: function(event, ui){
        populateInfo(ui.item)
        drawMap(ui.item.locations);
    }
});
