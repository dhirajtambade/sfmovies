from django.db import models

class Film(models.Model):
    name = models.CharField(max_length=128, db_index=True,
                            null=True, blank=True)
    year = models.IntegerField(null=True, blank=True)
    company = models.CharField(max_length=128, null=True, blank=True)
    director = models.CharField(max_length=64, null=True, blank=True)
    actors = models.CharField(max_length=256, null=True, blank=True)
    writers = models.CharField(max_length=256, null=True, blank=True)


class FilmLocations(models.Model):
    film = models.ForeignKey(Film, related_name="locations")
    location = models.CharField(max_length=128, null=True, blank=True)
    latitude = models.DecimalField(max_digits=10, decimal_places=6, null=True)
    longitude = models.DecimalField(max_digits=10, decimal_places=6, null=True)

    def __unicode__(self):
        return '%s' % self.location
