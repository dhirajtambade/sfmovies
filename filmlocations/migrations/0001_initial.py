# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Film',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(db_index=True, max_length=128, null=True, blank=True)),
                ('year', models.IntegerField(null=True, blank=True)),
                ('company', models.CharField(max_length=128, null=True, blank=True)),
                ('director', models.CharField(max_length=64, null=True, blank=True)),
                ('actors', models.CharField(max_length=256, null=True, blank=True)),
                ('writers', models.CharField(max_length=256, null=True, blank=True)),
            ],
        ),
        migrations.CreateModel(
            name='FilmLocations',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('location', models.CharField(max_length=128, null=True, blank=True)),
                ('film', models.ForeignKey(to='filmlocations.Film')),
            ],
        ),
    ]
