# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('filmlocations', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='filmlocations',
            name='latitude',
            field=models.DecimalField(null=True, max_digits=10, decimal_places=6),
        ),
        migrations.AddField(
            model_name='filmlocations',
            name='longitude',
            field=models.DecimalField(null=True, max_digits=10, decimal_places=6),
        ),
        migrations.AlterField(
            model_name='filmlocations',
            name='film',
            field=models.ForeignKey(related_name='locations', to='filmlocations.Film'),
        ),
    ]
