from rest_framework import serializers
from filmlocations.models import Film, FilmLocations

class FilmLocationsSerializer(serializers.ModelSerializer):
    class Meta:
        model = FilmLocations
        fields = (
            'location', 'latitude', 'longitude'
        )

class FilmSerializer(serializers.ModelSerializer):
    locations = FilmLocationsSerializer(many=True)

    class Meta:
        model = Film
        fields = (
            'name', 'year', 'company', 'director',
            'writers', 'actors', 'locations'
        )

