from django.conf.urls import url
from django.views.generic import TemplateView
from filmlocations import views

urlpatterns = [
    url(r'^$', TemplateView.as_view(template_name="index.html")),
    url(r'^autocomplete', views.FilmDetails.as_view()),
]
