
from rest_framework.views import APIView
from rest_framework.response import Response
from filmlocations.models import Film
from filmlocations.serializers import FilmSerializer


class FilmDetails(APIView):
    """
        Autocomplete film with its relevant details
    """
    def get(self, request):
        params = request.query_params.dict()
        films = Film.objects.filter(name__icontains=params['term'])[:10]
        serializer = FilmSerializer(films, many=True)

        film_list = []
        for film in serializer.data:
            film['label'] = film['name']
            film_list.append(film)

        return Response(film_list)


