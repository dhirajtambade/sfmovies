import unittest
import requests
import json

class AutocompleteTest(unittest.TestCase):
    def setUp(self):
        self.url = 'http://stark-spire-4846.herokuapp.com/film/autocomplete/'

    def test_autocomplete_get(self):
        """ Test Autocomplete API is working
        """
        params = {"term": "smile like"}
        response = requests.get(url=self.url, params=params).content
        res_json = json.loads(response)
        movie = res_json[0]
        self.assertEqual(movie["name"], "A Smile Like Yours")
        self.assertEqual(movie["year"], 1997)
        self.assertEqual(len(movie["locations"]), 4)

if __name__ == "__main__":
    unittest.main()

