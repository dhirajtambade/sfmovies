import requests
from filmlocations.models import FilmLocations
import json

URL = "http://maps.googleapis.com/maps/api/geocode/json?sensor=true&address="

def get_lat_long(LatLong):
    """
        Get Latitude and Longitude Info from Google API
    """
    locations = FilmLocations.objects.values_list('location', flat=True)
    for loc in set(locations):
        try:
            if loc in LatLong:
                continue
            url = URL + loc + ', San Francisco, CA'
            res = requests.get(url).content
            lat_long_info = json.loads(res)
            if lat_long_info["status"] == "OK":
                result = lat_long_info["results"][0]["geometry"]["location"]
                LatLong[loc] = result
        except:
            pass

    return LatLong

if __name__ == "__main__":
    f = open('filmlocations/data/LatLongData.json', 'r')
    LatLongData = json.loads(f.read())
    f.close()
    LatLongData = get_lat_long(LatLongData)
    with open('filmlocations/data/LatLongData.json', 'w') as outfile:
        json.dump(LatLongData, outfile, sort_keys=True, indent=4)
