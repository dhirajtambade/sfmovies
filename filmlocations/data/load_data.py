import csv
from filmlocations.models import Film, FilmLocations
import json

def load_data(file_name, LatLong):
    """
        Load data from files to database
        Args: file_name -> source csv name
              LatLong -> Longitude/Latitue Dictionory
    """
    csv_file = open(file_name, 'r')
    rows = csv.reader(csv_file, delimiter=',')
    next(rows, None)  # skip the headers

    for info in rows:
        actors = ', '.join(info[6:9])
        film, created = Film.objects.get_or_create(
            name=info[0].strip(),
            year=int(info[1])
        )
        if created:
            film.company = info[3]
            film.director = info[4]
            film.writers = info[5]
            film.actors = actors.strip(', ')
            film.save()

        location = info[2].strip()
        floc = FilmLocations.objects.create(film=film, location=location)

        if location in LatLong:
            floc.latitude = LatLong[location]["lat"]
            floc.longitude = LatLong[location]["lng"]
            floc.save()


if __name__ == "__main__":
    f = open('filmlocations/data/LatLongData.json', 'r')
    LatLongData = json.loads(f.read())
    f.close()

    filename = 'filmlocations/data/Film_Locations_in_San_Francisco.csv'
    load_data(filename, LatLongData)

